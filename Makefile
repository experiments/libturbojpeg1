CFLAGS = -std=c99 -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS += -fno-common \
	  -Wall \
	  -Wdeclaration-after-statement \
	  -Wextra \
	  -Wformat=2 \
	  -Winit-self \
	  -Winline \
	  -Wpacked \
	  -Wp,-D_FORTIFY_SOURCE=2 \
	  -Wpointer-arith \
	  -Wlarger-than-65500 \
	  -Wmissing-declarations \
	  -Wmissing-format-attribute \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wold-style-definition \
	  -Wredundant-decls \
	  -Wsign-compare \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wswitch-enum \
	  -Wundef \
	  -Wunreachable-code \
	  -Wunsafe-loop-optimizations \
	  -Wunused-but-set-variable \
	  -Wwrite-strings


LDLIBS += -lturbojpeg

libturbojpeg1-mem-dest: libturbojpeg1-mem-dest.o

clean:
	rm -rf *~ *.o *.jpg libturbojpeg1-mem-dest

test:
	valgrind --leak-check=full --show-reachable=yes ./libturbojpeg1-mem-dest
