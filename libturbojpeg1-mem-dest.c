/*
 * libturbojpeg1-mem-dest - an example about reusing the memory buffer with libturbojpeg1
 *
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <turbojpeg.h>

#define WIDTH 800
#define HEIGHT 480

static unsigned char *new_image(void)
{
	unsigned char *image;
	int i;

	image = malloc(WIDTH * HEIGHT * 3 * sizeof(*image));
	if (image == NULL) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < WIDTH * HEIGHT * 3; i++)
		image[i] = rand() % 0xff;

	return image;
}

int main(void)
{
	int i;
	tjhandle jpeg_compressor;
	unsigned char *out_buf;
	unsigned long out_buf_size;
	unsigned char *input_image;
	int ret;

	jpeg_compressor = tjInitCompress();
	if (jpeg_compressor == NULL) {
		fprintf(stderr, "tjInitCompress failed: %s\n", tjGetErrorStr());
		return -1;
	}

	out_buf_size = tjBufSize(WIDTH, HEIGHT, TJSAMP_422);
	ret = (int) out_buf_size;
	if (ret < 0) {
		fprintf(stderr, "tjBufSize failed: %s\n", tjGetErrorStr());
		goto out_destroy_compressor;
	}
	out_buf = malloc(out_buf_size);

	i = 0;
	while(i++ < 10) {
		input_image = new_image();

		ret = tjCompress2(jpeg_compressor, input_image, WIDTH, 0, HEIGHT,
				  TJPF_RGB, &out_buf, &out_buf_size, TJSAMP_422,
				  80, TJFLAG_NOREALLOC | TJFLAG_FASTDCT);
		if (ret < 0) {
			tjGetErrorStr();
			fprintf(stderr, "tjCompress2 failed: %s\n", tjGetErrorStr());
			free(input_image);
			goto out_free_buffer;
		}

		free(input_image);

		/* Is out_buf_size the number of bytes of the jpeg image, or the
		 * size of the allocated buffer? */
		fprintf(stderr, "out_buf: %p out_buf_size: %ld\n", out_buf, out_buf_size);
		{
			char filename[256] = { 0 };
			FILE *out_file;

			snprintf(filename, sizeof(filename), "out%03d.jpg", i);
			out_file = fopen(filename, "wb");
			fwrite(out_buf, out_buf_size, 1, out_file);
			fclose(out_file);
		}
	}

	ret = 0;

out_free_buffer:
	free(out_buf);
out_destroy_compressor:
	ret = tjDestroy(jpeg_compressor);
	if (ret < 0)
		fprintf(stderr, "tjDestroy failed: %s\n", tjGetErrorStr());

	return ret;
}
